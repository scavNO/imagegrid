/**
 * Responsible for letting the user define points
 * on a canvas to add new datapoints on which in turn
 * can be read by {@link select.js} to be clicked on.
 */

$(function() {

    var jQcanvas = $("#canvas");
    var canvas = jQcanvas[0]; //Fetches the raw DOM, instead of the JQuery one.

    var ctx = canvas.getContext("2d");
    var dataPoints = [];

    var imageGridCanvasUtil = new ImageGridCanvasUtil(ctx);
    imageGridCanvasUtil.drawCanvasWithDataPoints();



});