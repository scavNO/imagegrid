/**
 * Utility for selecting areas form a canvas.
 *
 * It is responsible for maintaining the canvas, adding
 * datapoints and keeping track of the mouse and whether
 * or not the mouse cursor is within a datapoint based on
 * the coordinates relative to the canvas and not the window.
 *
 * Finally, it enables the user to make a selection and have
 * the selection displayed and saved to a variable for further
 * use.
 *
 * @author Dag Østgulen Heradstveit
 */
$(function() {
    var jQcanvas = $("#canvas");
    var canvas = jQcanvas[0]; //Fetches the raw DOM, instead of the JQuery one.

    var ctx = canvas.getContext("2d");
    var dataPoints = [];

    //Some default response value?
    var selected = $('#selected');

    addDataPoints();

    var imageGridCanvasUtil = new ImageGridCanvasUtil(ctx);
    imageGridCanvasUtil.drawCanvasWithDataPoints(dataPoints);

    canvasTrackMouse();

    /**
     * Track position of the mouse to get the
     * X and Y coordinates within the canvas.
     */
    function canvasTrackMouse() {
        jQcanvas.mousemove(function(event) {
            var location = imageGridCanvasUtil.getPositon(event.pageX,event.pageY);
            $('#canvas_y').html(location.y);
            $('#canvas_x').html(location.x);
            fireEvent(location);
        });
    }

    /**
     * Every time the mouse is clicked, this even will check to
     * see whether or not the mouse is currently located within
     * any of the circles (dataPoint).
     *
     * @param location containing the x and y value for the mouse pointer
     */
    function fireEvent(location) {
        jQcanvas.mousedown(function() {
            dataPoints.forEach(function(dataPoint) {
                if(Math.pow(location.x - dataPoint.coordinates[0],2) + Math.pow(location.y -
                    dataPoint.coordinates[1],2) < Math.pow(dataPoint.coordinates[4],2)) {
                    selected.html(dataPoint.alternative + "(" + dataPoint.location + ")");
                }
            })
        });
    }

    /**
     * Dummy data. This should be replaced with an
     * actual self containing object which accepts all
     * the params needed to draw a circle and encapsulates
     * all the necessary functionality of the object.
     * E.g. click events.
     */
    function addDataPoints() {
        var dataPoint = {
            coordinates: [85, 285, 10, 0, 2 * Math.PI],
            alternative: 1,
            location: 'Knee'
        };

        dataPoints.push(dataPoint);

        dataPoint = {
            coordinates: [100, 210, 10, 0, 2 * Math.PI],
            alternative: 2,
            location: 'Scrotum'
        };

        dataPoints.push(dataPoint);

        dataPoint = {
            coordinates: [100, 25, 10, 0, 2 * Math.PI],
            alternative: 3,
            location: 'Head'
        };

        dataPoints.push(dataPoint);

        dataPoint = {
            coordinates: [165, 210, 10, 0, 2 * Math.PI],
            alternative: 4,
            location: 'Hand'
        };

        dataPoints.push(dataPoint);
    }
});