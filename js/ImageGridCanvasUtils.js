/**
 * This class is intended to hold all the common functionailty
 * required by the Canvas in this application.
 *
 * Currently it provides utility methods for drawing the canvas,
 * populating it  with dataPoints and tracking the mouse.
 *
 * @param ctx
 * @constructor
 * @author Dag Østgulen Heradstveit
 */

function ImageGridCanvasUtil(ctx) {

    console.log("Created common object");

    this.drawCanvasWithDataPoints = function(dataPoints) {
        console.log("Called drawCanvas");
        var img = new Image();
        img.onload = function() {
            ctx.drawImage(img, 0, 0);
            populateDataPoints(dataPoints);

        };
        img.src="img/body.gif";
    };

    /**
     * Get the cursor position relative to the canvas
     * instead of the window coordinates.
     * @param x window x coordinate
     * @param y window y coordinate
     * @returns {{x: number, y: number}}
     */
    this.getPositon = function(x,y) {
        var bbox = canvas.getBoundingClientRect();

        return {
            x: x - bbox.left * (canvas.width / bbox.width),
            y: y - bbox.top * (canvas.height / bbox.height)
        };
    };

    /**
     * Populate the given Canvas with datapoints.
     * Each canvas is taken from a specific structure.
     * @param dataPoints for the canvas
     */
    function populateDataPoints(dataPoints) {
        dataPoints.forEach(function(dataPoint) {
            console.log("Adding datapoint " + dataPoint.location);
            ctx.beginPath();
            ctx.fillStyle='#FF0000';
            ctx.arc(
                dataPoint.coordinates[0],
                dataPoint.coordinates[1],
                dataPoint.coordinates[2],
                dataPoint.coordinates[3],
                dataPoint.coordinates[4]);
            ctx.stroke();
            ctx.fill();
        });
    }

}